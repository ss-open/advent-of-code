package main

import (
	"bufio"
	"fmt"
	"os"
	"reflect"
	"strconv"
	"strings"
)

type almanacMap struct {
	destinationRangeStart int
	sourceRangeStart      int
	sourceRangeLength     int
}

type almanacMaps struct {
	in   string
	out  string
	maps []almanacMap
}

func getSeeds(seedsLine string) [][]int {
	seedsLineParts := strings.Fields(strings.Split(seedsLine, ": ")[1])
	var seeds [][]int

	for i := 0; i < len(seedsLineParts); i += 2 {
		seedStart, err := strconv.Atoi(seedsLineParts[i])
		if err != nil {
			panic(fmt.Sprintf("Unable to convert seed start number '%s' onto integer", seedsLineParts[i]))
		}

		seedLength, err := strconv.Atoi(seedsLineParts[i+1])
		if err != nil {
			panic(fmt.Sprintf("Unable to convert seed length number '%s' onto integer", seedsLineParts[i+1]))
		}

		seeds = append(seeds, []int{seedStart, seedStart + seedLength - 1})
	}

	return seeds
}

func getSeedsFromAlmanacMaps(seeds [][]int, maps []almanacMap) [][]int {
	var mappedSeeds [][]int

SeedsPairsIteration:
	for _, s := range seeds {
		fmt.Printf("\tinitial seeds pair: %v\n", s)
		for _, m := range maps {
			fmt.Printf("\t\tseeds pair: %v\n", s)
			fmt.Printf("\t\tmap: %v\n", m)
			mapSourceRangeStop := m.sourceRangeStart + m.sourceRangeLength
			fmt.Printf("\t\tmapSourceRangeStop: %d\n", mapSourceRangeStop)
			if s[0] >= m.sourceRangeStart && s[0] < mapSourceRangeStop || s[1] >= m.sourceRangeStart && s[1] < mapSourceRangeStop {
				fmt.Printf("\t\tmatch!\n")
				seedPairRest := make([]int, 2)
				copy(seedPairRest, s)

				var seedPairMin int
				if s[0] < m.sourceRangeStart {
					seedPairMin = m.sourceRangeStart
					seedPairRest[1] = seedPairMin - 1
				} else {
					seedPairMin = s[0]
				}

				var seedPairMax int
				if s[1] >= mapSourceRangeStop {
					seedPairMax = mapSourceRangeStop
					seedPairRest[0] = seedPairMax + 1
				} else {
					seedPairMax = s[1]
				}

				convertedSeedsPair := []int{
					m.destinationRangeStart + seedPairMin - m.sourceRangeStart,
					m.destinationRangeStart + seedPairMax - m.sourceRangeStart,
				}
				fmt.Printf(
					"\t\tconverting seeds pair %v to %v\n",
					[]int{seedPairMin, seedPairMax},
					convertedSeedsPair,
				)
				mappedSeeds = append(mappedSeeds, convertedSeedsPair)

				if reflect.DeepEqual(s, seedPairRest) {
					continue SeedsPairsIteration
				}

				s[0] = seedPairRest[0]
				s[1] = seedPairRest[1]
				continue
			}
			fmt.Printf("\tseeds pair rest: %v\n", s)
		}

		if s[0] < s[1] {
			fmt.Printf("\t\tseeds pair not converted: %v\n", s)
			mappedSeeds = append(mappedSeeds, []int{
				s[0],
				s[1],
			})
		}
	}

	return mappedSeeds
}

func getAlmanacMaps(almanacMapsLine string) almanacMaps {
	almanacMapsDests := strings.Split(strings.Split(almanacMapsLine, " ")[0], "-to-")

	return almanacMaps{
		in:  almanacMapsDests[0],
		out: almanacMapsDests[1],
	}
}

func getAlmanacMap(almanacMapLine string) almanacMap {
	almanacMapsNumbers := strings.Fields(almanacMapLine)

	destinationRangeStart, err := strconv.Atoi(almanacMapsNumbers[0])
	if err != nil {
		panic(fmt.Sprintf("Unable to convert '%s' onto integer", almanacMapsNumbers[0]))
	}

	sourceRangeStart, err := strconv.Atoi(almanacMapsNumbers[1])
	if err != nil {
		panic(fmt.Sprintf("Unable to convert '%s' onto integer", almanacMapsNumbers[1]))
	}

	sourceRangeLength, err := strconv.Atoi(almanacMapsNumbers[2])
	if err != nil {
		panic(fmt.Sprintf("Unable to convert '%s' onto integer", almanacMapsNumbers[2]))
	}

	return almanacMap{
		destinationRangeStart: destinationRangeStart,
		sourceRangeStart:      sourceRangeStart,
		sourceRangeLength:     sourceRangeLength,
	}
}

func main() {
	readFile, err := os.Open(os.Args[1])

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	var currentAlmanacMaps almanacMaps
	var almanacMapsList []almanacMaps
	var seeds [][]int
	for fileScanner.Scan() {
		lineText := fileScanner.Text()
		if lineText == "" {
			continue
		}

		if strings.HasPrefix(lineText, "seeds: ") {
			seeds = getSeeds(lineText)
			continue
		}

		if strings.HasSuffix(lineText, "map:") {
			if currentAlmanacMaps.in != "" {
				almanacMapsList = append(almanacMapsList, currentAlmanacMaps)
			}
			currentAlmanacMaps = getAlmanacMaps(lineText)
			continue
		}

		currentAlmanacMaps.maps = append(currentAlmanacMaps.maps, getAlmanacMap(lineText))
	}
	almanacMapsList = append(almanacMapsList, currentAlmanacMaps)

	fmt.Printf("Initial seeds: %v\n", seeds)
	for _, almanacMapsListElement := range almanacMapsList {
		fmt.Printf(
			"%s -> %s: %v\n",
			almanacMapsListElement.in,
			almanacMapsListElement.out,
			almanacMapsListElement.maps,
		)

		seeds = getSeedsFromAlmanacMaps(seeds, almanacMapsListElement.maps)
		fmt.Println(seeds)
	}
	fmt.Printf("Computed seeds: %v\n", seeds)

	var lowestLocationNumber int
	for _, seed := range seeds {
		if seed[0] < lowestLocationNumber || lowestLocationNumber == 0 {
			lowestLocationNumber = seed[0]
		}
	}
	fmt.Println(lowestLocationNumber)
}
