package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"time"
)

func ensureMemoryLimit(limit uint64, frequency time.Duration) {
	var m runtime.MemStats
	for {
		runtime.ReadMemStats(&m)

		memoryInMib := m.TotalAlloc / 1024 / 1024

		// fmt.Printf("Mem consumption: %d MiB\n", memoryInMib)
		if memoryInMib > limit {
			fmt.Println("OOM!")
			os.Exit(1)
		}

		time.Sleep(1 * frequency)
	}
}

type hand struct {
	cards string
	bid   int
}

var cardTypes = []byte{
	'J',
	'2',
	'3',
	'4',
	'5',
	'6',
	'7',
	'8',
	'9',
	'T',
	'Q',
	'K',
	'A',
}

func getCardTypesCount(handCards string) map[byte]int {
	cardTypesCount := map[byte]int{}

	for c := 0; c < len(handCards); c++ {
		cardTypesCount[handCards[c]] += 1
	}

	return cardTypesCount
}

// Five of a kind, where all five cards have the same label: AAAAA
func isFiveOfAKind(handCards string) bool {
	firstCard := handCards[0]

	for c := 1; c < len(handCards); c++ {
		if handCards[c] != firstCard {
			return false
		}
	}

	return true
}

// Four of a kind, where four cards have the same label and one card has a different label: AA8AA
func isFourOfAKind(handCards string) bool {
	cardTypesCount := getCardTypesCount(handCards)

	if len(cardTypesCount) == 2 {
		for _, count := range cardTypesCount {
			if count == 4 {
				return true
			}
		}
	}

	return false
}

// Full house, where three cards have the same label, and the remaining two cards share a different label: 23332
func isFullHouse(handCards string) bool {
	cardTypesCount := getCardTypesCount(handCards)

	if len(cardTypesCount) == 2 {
		for _, count := range cardTypesCount {
			if count == 3 {
				return true
			}
		}
	}

	return false
}

// Three of a kind, where three cards have the same label, and the remaining two cards are each different from any other card in the hand: TTT98
func isThreeOfAKind(handCards string) bool {
	cardTypesCount := getCardTypesCount(handCards)

	for _, count := range cardTypesCount {
		if count == 3 {
			return true
		}
	}

	return false
}

// Two pair, where two cards share one label, two other cards share a second label, and the remaining card has a third label: 23432
func isTwoPair(handCards string) bool {
	cardTypesCount := getCardTypesCount(handCards)
	var pairsCount int

	for _, count := range cardTypesCount {
		if count == 2 {
			pairsCount++
		}
		if pairsCount == 2 {
			return true
		}
	}

	return false
}

// One pair, where two cards share one label, and the other three cards have a different label from the pair and each other: A23A4
func isOnePair(handCards string) bool {
	cardTypesCount := getCardTypesCount(handCards)

	for _, count := range cardTypesCount {
		if count == 2 {
			return true
		}
	}

	return false
}

// High card, where all cards' labels are distinct: 23456
func isHighCard(handCards string) bool {
	return len(getCardTypesCount(handCards)) == 5
}

type handTypeChecker func(string) bool

func getHandTypeLevel(handCards string) int {
	handTypeCheckers := []handTypeChecker{
		isHighCard,
		isOnePair,
		isTwoPair,
		isThreeOfAKind,
		isFullHouse,
		isFourOfAKind,
		isFiveOfAKind,
	}

	var currentLevel = 0
	for level, handTypeChecker := range handTypeCheckers {
		if handTypeChecker(handCards) {
			currentLevel = level + 1
		}
	}

	return currentLevel
}

func getHigherHandTypeLevel(handCards string) int {
	var currentLevel = 0

	for c := 1; c < len(cardTypes); c++ {
		level := getHandTypeLevel(strings.ReplaceAll(handCards, "J", string(cardTypes[c])))
		if level > currentLevel {
			currentLevel = level
		}
	}

	return currentLevel
}

func less(hand1 hand, hand2 hand) bool {
	hand1TypeLevel := getHigherHandTypeLevel(hand1.cards)
	hand2TypeLevel := getHigherHandTypeLevel(hand2.cards)

	if hand1TypeLevel == hand2TypeLevel {
		for h := 0; h < len(hand1.cards); h++ {
			card1IndexByte := bytes.IndexByte(cardTypes, hand1.cards[h])
			card2IndexByte := bytes.IndexByte(cardTypes, hand2.cards[h])

			if card1IndexByte == card2IndexByte {
				continue
			}

			return card1IndexByte < card2IndexByte
		}

		return false
	}

	return hand1TypeLevel < hand2TypeLevel
}

func main() {
	go ensureMemoryLimit(512, 1*time.Microsecond)
	readFile, err := os.Open(os.Args[1])

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	var hands []hand
	for fileScanner.Scan() {
		lineText := fileScanner.Text()
		lineTextTab := strings.Fields(lineText)

		handBid, err := strconv.Atoi(lineTextTab[1])
		if err != nil {
			panic(fmt.Sprintf("Unable to convert bid '%s' to int", lineTextTab[1]))
		}

		hands = append(hands, hand{
			cards: lineTextTab[0],
			bid:   handBid,
		})
	}

	sort.Slice(hands, func(i, j int) bool {
		return less(hands[i], hands[j])
	})

	score := 0
	for h, hand := range hands {
		score += (h + 1) * hand.bid
	}
	fmt.Println(score)
}
