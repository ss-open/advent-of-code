package main

import (
	"bufio"
	"fmt"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"
)

func ensureMemoryLimit(limit uint64, frequency time.Duration) {
	var m runtime.MemStats
	for {
		runtime.ReadMemStats(&m)

		memoryInMib := m.TotalAlloc / 1024 / 1024

		fmt.Printf("Mem consumption: %d MiB\n", memoryInMib)
		if memoryInMib > limit {
			fmt.Println("OOM!")
			os.Exit(1)
		}

		time.Sleep(1 * frequency)
	}
}

func getNumber(numbersLine string) int {
	numberStr := strings.Join(strings.Fields(numbersLine), "")

	number, err := strconv.Atoi(numberStr)
	if err != nil {
		panic(fmt.Sprintf("Could not convert number '%s' to int", numberStr))
	}

	return number
}

func getTraveledDistance(elapsedTime int, buttonTime int) int {
	traveledDistance := (elapsedTime - buttonTime) * buttonTime

	return traveledDistance
}

func main() {
	go ensureMemoryLimit(512, 1*time.Microsecond)
	readFile, err := os.Open(os.Args[1])

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	var time int
	var distance int
	for fileScanner.Scan() {
		lineText := fileScanner.Text()
		if strings.HasPrefix(lineText, "Time: ") {
			time = getNumber(strings.Split(lineText, ":")[1])
			continue
		}

		if strings.HasPrefix(lineText, "Distance: ") {
			distance = getNumber(strings.Split(lineText, ":")[1])
			continue
		}
	}

	var beatenRecordMin int
	for t := 0; t <= time; t++ {
		traveledDistance := getTraveledDistance(time, t)
		if traveledDistance > distance {
			beatenRecordMin = t
			break
		}
	}

	var beatenRecordMax int
	for t := time - 1; t > 0; t-- {
		traveledDistance := getTraveledDistance(time, t)
		if traveledDistance > distance {
			beatenRecordMax = t
			break
		}
	}

	fmt.Println(
		time,
		distance,
		beatenRecordMin,
		beatenRecordMax,
		beatenRecordMax-beatenRecordMin+1,
	)
}
