package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	literalNumbers := map[string]string{
		"one":   "1",
		"two":   "2",
		"three": "3",
		"four":  "4",
		"five":  "5",
		"six":   "6",
		"seven": "7",
		"eight": "8",
		"nine":  "9",
	}
	counter := 0
	readFile, err := os.Open("input")

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		lineText := fileScanner.Text()
		var firstDigit string
		var lastDigit string

		for i := 0; i < len(lineText); i++ {
			char := string(lineText[i])

			for literalNumber := range literalNumbers {
				j := 0
				for ; j < len(literalNumber); j++ {
					if i+j >= len(lineText) || string(lineText[i+j]) != string(literalNumber[j]) {
						break
					}
				}

				if j != len(literalNumber) {
					continue
				}

				char = literalNumbers[literalNumber]
				i += j - 2
				break
			}

			_, err := strconv.Atoi(char)
			if err != nil {
				continue
			}

			if firstDigit == "" {
				firstDigit = char
				continue
			}

			lastDigit = char
		}

		if lastDigit == "" {
			lastDigit = firstDigit
		}

		number, err := strconv.Atoi(firstDigit + lastDigit)
		if err != nil {
			panic(
				fmt.Sprintf(
					"Number conversion fail on line '%s' with '%s' and '%s'",
					lineText,
					firstDigit,
					lastDigit,
				),
			)
		}

		fmt.Printf("%s: %d\n", lineText, number)
		counter += number
	}

	readFile.Close()

	fmt.Println(counter)
}
