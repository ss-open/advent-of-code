package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type specialChar struct {
	name byte
	x    int
	y    int
}

type partNumber struct {
	number      int
	specialChar specialChar
}

func isSpecialChar(char byte) bool {
	return char != '.' && (char < '0' || char > '9')
}

func main() {
	var schematicMapping = [][]byte{}
	var partNumbers = []partNumber{}
	readFile, err := os.Open(os.Args[1])

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		lineText := fileScanner.Text()
		schematicMapping = append(schematicMapping, []byte(lineText))
	}

	for y := 0; y < len(schematicMapping); y++ {
		for x := 0; x < len(schematicMapping[y]); x++ {
			_, err := strconv.Atoi(string(schematicMapping[y][x]))

			if err == nil {
				i := x + 1
				for ; i < len(schematicMapping[y]); i++ {
					if _, err := strconv.Atoi(string(schematicMapping[y][i])); err != nil {
						break
					}
				}

				var specialChar specialChar
			SpecialCharScan:
				for j := x; j < i; j++ {
					for sY := -1; sY <= 1; sY++ {
						if y+sY < 0 || y+sY >= len(schematicMapping) {
							continue
						}

						for sX := -1; sX <= 1; sX++ {
							if j+sX < 0 || j+sX >= len(schematicMapping[y+sY]) {
								continue
							}

							if isSpecialChar(schematicMapping[y+sY][j+sX]) {
								specialChar.name = schematicMapping[y+sY][j+sX]
								specialChar.x = j + sX
								specialChar.y = y + sY
								break SpecialCharScan
							}
						}
					}
				}

				if specialChar.name != 0 {
					number, err := strconv.Atoi(string(schematicMapping[y])[x:i])
					if err != nil {
						panic(fmt.Sprintf("Unable to parse number from %s (%d:%d)", schematicMapping[y][x:i], x, i))
					}

					partNumbers = append(partNumbers, partNumber{
						number:      number,
						specialChar: specialChar,
					})
				}

				x = i - 1
			}
		}
	}

	readFile.Close()

	fmt.Println(partNumbers)
	counter := 0
	var countedPartIndexes = []int{}
PartCounting:
	for partIndex, partNumber := range partNumbers {
		fmt.Printf("Counter status: %d\n", counter)
		fmt.Printf("Part number: %d\n", partNumber.number)
		for _, countedPartIndex := range countedPartIndexes {
			if partIndex == countedPartIndex {
				fmt.Printf("\tAlready counted, skipping\n")
				continue PartCounting
			}
		}
		fmt.Println("\tEntering sum process..")
		countedPartIndexes = append(countedPartIndexes, partIndex)

		if partNumber.specialChar.name == '*' {
			fmt.Printf("\tGear ratio detected, multiplying...\n")
			gearRatio := 1
			gearRatioPartsCount := 0
			for ratioPartIndex, ratioPartNumber := range partNumbers {
				if ratioPartNumber.specialChar.x == partNumber.specialChar.x &&
					ratioPartNumber.specialChar.y == partNumber.specialChar.y {
					fmt.Printf("\t\tMultiplying by %d\n", ratioPartNumber.number)
					gearRatio *= ratioPartNumber.number
					gearRatioPartsCount++
					countedPartIndexes = append(countedPartIndexes, ratioPartIndex)
				}
			}
			fmt.Printf("\t\tGear ratio: %d\n", gearRatio)

			if gearRatioPartsCount > 1 {
				counter += gearRatio
			}
		}
	}
	fmt.Println(counter)
}
