package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func getWinningNumbers(cardLineNumbers string) []string {
	cardLineNumbersTab := strings.Split(cardLineNumbers, " | ")
	winningNumbers := strings.Fields(cardLineNumbersTab[0])
	myNumbers := strings.Fields(cardLineNumbersTab[1])
	var myWinningNumbers = []string{}

	for _, winningNumber := range winningNumbers {
		for _, myNumber := range myNumbers {
			if myNumber == winningNumber {
				myWinningNumbers = append(myWinningNumbers, myNumber)
			}
		}
	}

	return myWinningNumbers
}

type scratchCard struct {
	winningNumbersCount int
	copies              int
}

func main() {
	readFile, err := os.Open(os.Args[1])

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	var scratchCards = []scratchCard{}
	for fileScanner.Scan() {
		lineText := fileScanner.Text()
		fmt.Println(lineText)
		cardLineTab := strings.Split(lineText, ": ")
		myWinningNumbers := getWinningNumbers(cardLineTab[1])
		winningNumbersCount := len(myWinningNumbers)
		fmt.Printf(
			" => winning numbers: %s (%d)\n",
			myWinningNumbers,
			winningNumbersCount,
		)
		scratchCards = append(scratchCards, scratchCard{
			winningNumbersCount: winningNumbersCount,
			copies:              1,
		})
	}

	for s, scratchCard := range scratchCards {
		for c := 0; c < scratchCard.copies; c++ {
			for w := 0; w < scratchCard.winningNumbersCount; w++ {
				scratchCards[w+s+1].copies += 1
			}
		}
	}

	fmt.Println(scratchCards)

	counter := 0
	for _, scratchCard := range scratchCards {
		counter += scratchCard.copies
	}

	fmt.Println(counter)
}
