package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var bagContent = map[string]int{
	"red":   12,
	"green": 13,
	"blue":  14,
}

func getSubsetsCube(subsetCubeLint string) (string, int) {
	subsetsCube := strings.Split(subsetCubeLint, " ")
	subsetsCubeName := subsetsCube[1]
	subsetsCubeCount, err := strconv.Atoi(subsetsCube[0])
	if err != nil {
		panic(fmt.Sprintf("Unable to get subsets cube count for %s", subsetCubeLint))
	}

	return subsetsCubeName, subsetsCubeCount
}

func isGamePossible(gameSetLine string) bool {
	subsets := strings.Split(gameSetLine, "; ")

	for i := 0; i < len(subsets); i++ {
		subsetsCubes := strings.Split(subsets[i], ", ")
		for j := 0; j < len(subsetsCubes); j++ {
			subsetsCubeName, subsetsCubeCount := getSubsetsCube(subsetsCubes[j])

			if subsetsCubeCount > bagContent[subsetsCubeName] {
				return false
			}
		}
	}

	return true
}

func getGameSetPower(gameSetLine string) int {
	var minimumBagContent = map[string]int{}
	subsets := strings.Split(gameSetLine, "; ")

	for i := 0; i < len(subsets); i++ {
		subsetsCubes := strings.Split(subsets[i], ", ")
		for j := 0; j < len(subsetsCubes); j++ {
			subsetsCubeName, subsetsCubeCount := getSubsetsCube(subsetsCubes[j])

			if subsetsCubeCount > minimumBagContent[subsetsCubeName] {
				minimumBagContent[subsetsCubeName] = subsetsCubeCount
			}
		}
	}

	power := 1
	for _, minimumBagValue := range minimumBagContent {
		power *= minimumBagValue
	}

	return power
}

func main() {
	counterOfPossibilities := 0
	counterOfPower := 0
	readFile, err := os.Open("input")

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		lineText := fileScanner.Text()
		gameLine := strings.Split(lineText, ": ")

		gameId, err := strconv.Atoi(strings.Split(gameLine[0], " ")[1])
		if err != nil {
			panic(fmt.Sprintf("Unable to get game ID from line: %s", lineText))
		}

		counterOfPower += getGameSetPower(gameLine[1])

		if isGamePossible(gameLine[1]) == false {
			continue
		}

		counterOfPossibilities += gameId
	}

	readFile.Close()

	fmt.Println(counterOfPossibilities)
	fmt.Println(counterOfPower)
}
